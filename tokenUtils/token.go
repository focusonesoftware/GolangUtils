package tokenUtils

import (
	"crypto/rsa"
	"io/ioutil"
	"net/http"
	"time"

	context "github.com/kataras/iris/context"

	jwt "github.com/dgrijalva/jwt-go"
	jwtmiddleware "github.com/iris-contrib/middleware/jwt"
	"github.com/kataras/iris"
	"github.com/kataras/iris/core/router"
	//logger "gitlab.com/focusonesoftware/GolangLogger"
)

type ErrorCode string

const (
	ErrCodeGeneralErrorOrUnhandled ErrorCode = "E000-000"
)

type TokenResponse struct {
	ResultCode string      `json:"result_code"`
	ResultTime string      `json:"result_time,omitempty"`
	ErrorCode  ErrorCode   `json:"error_code,omitempty"`
	Message    string      `json:"message,omitempty"`
	Data       interface{} `json:"data,omitempty"`
}

func NewTokenResponse(
	httpStatusCode int,
	errorCode ErrorCode,
	message string,
	data interface{},
) *TokenResponse {
	return &TokenResponse{
		ResultCode: http.StatusText(httpStatusCode),
		ResultTime: GetCurrentDateTime(),
		ErrorCode:  errorCode,
		Message:    message,
		Data:       data,
	}
}

func readTokenKeys(pubKeyPath string) (*rsa.PublicKey, error) {
	var err error

	key, err := ioutil.ReadFile(pubKeyPath)
	if err != nil {
		//logger.Error.Println(err.Error())
		return nil, err
	}

	publicKey, err := jwt.ParseRSAPublicKeyFromPEM(key)
	if err != nil {
		return nil, err
	}

	return publicKey, nil
}

func ReadTokenKeys(pubKeyPath string) (*rsa.PublicKey, error) {
	var err error

	key, err := ioutil.ReadFile(pubKeyPath)
	if err != nil {
		//logger.Error.Println(err.Error())
		return nil, err
	}

	publicKey, err := jwt.ParseRSAPublicKeyFromPEM(key)
	if err != nil {
		return nil, err
	}

	return publicKey, nil
}

// // OnError default error handler
// func onError(ctx context.Context, err string) {
// 	ctx.Header("Access-Control-Allow-Origin", "*")
// 	// ctx.StatusCode(iris.StatusUnauthorized)
// 	// ctx.Writef(err)

// 	res := NewTokenResponse(
// 		iris.StatusUnauthorized,
// 		ErrCodeGeneralErrorOrUnhandled,
// 		err,
// 		nil,
// 	)

// 	ctx.StatusCode(iris.StatusUnauthorized)
// 	ctx.JSON(res)
// }

func onError(ctx context.Context, err error) {
	ctx.Header("Access-Control-Allow-Origin", "*")
	// ctx.StatusCode(iris.StatusUnauthorized)
	// ctx.Writef(err)

	res := NewTokenResponse(
		iris.StatusUnauthorized,
		ErrCodeGeneralErrorOrUnhandled,
		err.Error(),
		nil,
	)

	ctx.StatusCode(iris.StatusUnauthorized)
	ctx.JSON(res)
}

func SetupAppTokenKeys(app *iris.Application, pubKeyPath string) {
	jwtHandler := jwtmiddleware.New(jwtmiddleware.Config{
		ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
			return readTokenKeys(pubKeyPath)
		},
		// When set, the middleware verifies that tokens are signed with the specific signing algorithm
		// If the signing method is not constant the ValidationKeyGetter callback can be used to implement additional checks
		// Important to avoid security issues described here: https://auth0.com/blog/2015/03/31/critical-vulnerabilities-in-json-web-token-libraries/
		// SigningMethod: jwt.SigningMethodRS256,
		ErrorHandler: onError,
	})

	app.Use(jwtHandler.Serve)
}

func SetupPartyTokenKeys(party router.Party, pubKeyPath string) {
	jwtHandler := jwtmiddleware.New(jwtmiddleware.Config{
		ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
			return readTokenKeys(pubKeyPath)
		},
		// When set, the middleware verifies that tokens are signed with the specific signing algorithm
		// If the signing method is not constant the ValidationKeyGetter callback can be used to implement additional checks
		// Important to avoid security issues described here: https://auth0.com/blog/2015/03/31/critical-vulnerabilities-in-json-web-token-libraries/
		// SigningMethod: jwt.SigningMethodRS256,
		ErrorHandler: onError,
	})

	party.Use(jwtHandler.Serve)
}

// func keyLookupFunc(token *jwt.Token) (interface{}, error) {
// 	// Don't forget to validate the alg is what you expect:
// 	if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
// 		return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
// 	}

// 	// Look up key
// 	key, err := lookupPublicKey(token.Header["kid"])
// 	if err != nil {
// 		return nil, err
// 	}

// 	// Unpack key from PEM encoded PKCS8
// 	return jwt.ParseRSAPublicKeyFromPEM(key)
// }

func timeIn(t time.Time, name string) (time.Time, error) {
	loc, err := time.LoadLocation(name)

	if err == nil {
		t = t.In(loc)
	}

	return t, err
}

func GetCurrentDateTime() string {
	currentDateTime, _ := timeIn(time.Now(), "Asia/Bangkok")

	return currentDateTime.Format(time.RFC3339)
}
